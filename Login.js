import {useState, useEffect} from "react";
import {
    Container,
    Row,
    Col,
    Form,
    Button

} from "react-bootstrap"

export default function Login(){

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const [isActive, setIsActive] = useState(true)

    function Login(e){
        e.preventDefault()

        alert("Successful Login!")
    }

    useEffect( () => {

        if(email != "" && password != "" ){
            setIsActive(false)
        }
    }, [email, password])

    return(
        <Container className="my-5">
            <Row className="justify-content-center">
                <Col xs={10} md={6}>
                    <Form 
                        className="border p-3" 
                        onSubmit={(e) => Login(e)}>

                        {/*email*/}
                      <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control 
                            type="email" 
                            placeholder="Enter email" 
                            value={email}
                            onChange={ (e) => {
                                setEmail(e.target.value)
                                // console.log(e.target.value)
                            }}
                        />
                      </Form.Group>

                        {/*password*/}
                      <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control 
                            type="password" 
                            placeholder="Password" 
                            value={password}
                            onChange={(e) => {
                                setPassword(e.target.value)
                            }}
                        />
                      </Form.Group>

                    {/*submit button*/}
                        <Button 
                            variant="primary" 
                            type="submit"
                            disabled={isActive}
                        >
                            Submit
                        </Button>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}
